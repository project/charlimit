$Id

The Character Limit module enables the restriction of minimum and maximum
characters for any text field in any Drupal form (words limit coming soon...)

Installation
------------
1) Copy the charlimit directory to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)

Configuration
-------------

Go to Administer -> Site configuration -> Character Limit (/admin/settings/charlimit)

Support
-------

Please post bug reports and feature requests on the module's
drupal.org project page.

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt

Credits / Contact
-----------------

The module was developed by Alon Pe'er (alonpeer).
Find my contact details at http://alonpeer.com 